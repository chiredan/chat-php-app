<?php


use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    const VERSION = parent::VERSION.'WoE';

    public function registerBundles()
    {
        $bundles = [
            new WorldOfEquestria\AppBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
        ];

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }

    /**
     * Checks if a given class name belongs to an active bundle.
     *
     * @param string $class A class name
     *
     * @return bool true if the class belongs to an active bundle, false otherwise
     *
     * @api
     *
     * @deprecated since version 2.6, to be removed in 3.0.
     */
    public function isClassInActiveBundle($class)
    {
        // TODO: Implement isClassInActiveBundle() method.
    }
}