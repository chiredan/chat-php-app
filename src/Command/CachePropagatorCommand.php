<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 20.03.2016
 * Time: 01:08
 */

namespace WorldOfEquestria\Command;


use Predis\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Thruway\CallResult;
use WorldOfEquestria\Service\RpcClient;
use WorldOfEquestria\Service\SubscriptionClient;

class CachePropagatorCommand extends WorkerCommand
{
    const COMMAND_NAME = 'worldofequestria:cache_propagator';
    const CHANNEL_NAME = 'wamp.subscription.on_subscribe';

    /** @var  Cache */
    private $cache;

    /**
     * @var SubscriptionClient
     */
    private $client;


    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->client = $this->getContainer()->get('subscription_client');
        $provider = $this->client->getProvider();
        $provider
            ->registerProcedure(self::CHANNEL_NAME, $this->getSubscribeListenerCallback());
        $this->client->start();
    }

    private function getSubscribeListenerCallback() {
        /**
         * @var RpcClient $client
         * @var Client $redis
         */
        $client = $this->getContainer()->get('subscription_client');
        $client->setClient($this->client->getClient());
        $redis = $this->getContainer()->get('snc_redis.cache');

        return function ($args) use ($client, $redis) {
            $session = $client->getClient()->getSession();

            $sessionId = $args[0];
            $subscriptionId = json_encode($args[1]);

            $callback = function(CallResult $result) use ($sessionId,$subscriptionId, $session, $redis) {
                $uri = $result->getArguments()[0]->uri;

                $cachedMessages = $redis->get(CacheCommand::CACHE_PREFIX.$uri);
                $messages = json_decode($cachedMessages, true);

                if(count($messages) > 20) {
                    $messages = array_slice($messages, -20, 20);
                }


                if(is_array($messages) || empty($messages)){
                    foreach($messages as $message)
                        $session->publish($uri, [$message], []);//, ['eligible'=>[(int)$subscriptionId]]);
                }
            };

            $call = $session->call('wamp.subscription.get', [$args[1]])->then(
                $callback, function ($error) {echo "Call Error: {$error}\n";}
            );

            return true;
        };
    }
}