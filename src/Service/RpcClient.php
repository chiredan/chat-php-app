<?php

namespace WorldOfEquestria\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use WorldOfEquestria\Websocket\ThruwayClient;

class RpcClient extends ThruwayClient
{
    private $procedureProvider;

    public function __construct($realm, $address, $port, ProcedureProvider $pc)
    {
        parent::__construct($realm, $address, $port);
        $this->procedureProvider = $pc;
    }

    public function getProvider() {
        return $this->procedureProvider;
    }

    public function start(){
        parent::startClient($this->getProvider());
    }
}