<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 19.03.2016
 * Time: 17:27
 */

namespace WorldOfEquestria\Command;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class WorkerCommand extends ContainerCommand
{
    const MAIN_PID_ROOTNAME = 'process';

    /**
     * @var integer
     */
    private $pid;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->pid = posix_getpid();

        $this->destroyPidFile();
        $this->storeProcessData('process', [
            'pid'  => $this->getPid(),
            'name' => $this->getName(),
            'args' => $input->getArguments()
        ] );
    }

    /**
     * @return integer
     */
    protected function getPid() {
        return $this->pid;
    }

    /**
     * @param string|array $key
     * @param mixed|null $data
     * @return integer|bool
     */
    protected function storeProcessData($key, $data = null) {

        if(is_array($key) && null !== $data) {
            throw new \InvalidArgumentException('Second value must be null when the first is array');
        } elseif(null === $data) {
            throw new \InvalidArgumentException('Second value cannot be null when the first is string');
        }

        if($key === self::MAIN_PID_ROOTNAME) {
            return file_put_contents($this->getProcessFile(), json_encode($data));
        }

        $processData = json_decode(file_get_contents($this->getProcessFile()), true);

        if(is_array($key)) {

            $processData = json_encode(array_merge($processData, $key));

            return file_put_contents($this->getProcessFile(), $processData);
        }

        $processData = json_encode(array_merge($processData, [$key => $data]));

        return file_put_contents($this->getProcessFile(), $processData);
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getProcessData($key) {

        $processData = json_decode(file_get_contents($this->getProcessFile()), true);

        if($key === self::MAIN_PID_ROOTNAME) {
            return $processData;
        }

        return $processData[$key];
    }

    private function destroyPidFile() {

        if(false === file_exists($this->getProcessFile())) {
            return false;
        }

        return unlink($this->getProcessFile());
    }

    /**
     * @return string
     */
    private function getProcessFile() {
        return sprintf('/tmp/%s_%s.pid',$this->getName(),$this->getPid());
    }

    public function __destruct()
    {
        $this->destroyPidFile();
    }
}
