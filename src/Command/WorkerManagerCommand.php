<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 28.02.2016
 * Time: 00:48
 */

namespace WorldOfEquestria\Command;


use WorldOfEquestria\Command\PublisherCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

//TODO przerobienie na interaktywny propagator komend

class WorkerManagerCommand extends ContainerCommand
{
    const COMMAND_NAME = 'worldofequestria:worker_manager';
    const WORKER_COUNT = 16; //TODO do configa
    //TODO 2 generator dla parameters yml

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pattern = new Process(sprintf('php %s %s',$this->getAppDir(), PublisherCommand::COMMAND_NAME));
        $processes = [];

        while(true){
            while(count($processes) < self::WORKER_COUNT){
                $clone = clone($pattern);
                $clone->start();

                $processes[] = $clone;
            }

            /**
             * @var int $id
             * @var Process $process
             */
            foreach($processes as $id => $process){
                if($process->isRunning()){
                    continue;
                }

                $output->writeln(sprintf('<error>Worker %s died</error>', $id));
                unset($processes[$id]);
            }

            sleep(1);
        }
    }

    private function getAppDir() {
        return $this->getContainer()->get('kernel')->getRootDir().'/app.php';
    }
}