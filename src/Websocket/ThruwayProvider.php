<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 19.02.2016
 * Time: 23:14
 */

namespace WorldOfEquestria\Websocket;


use Thruway\ClientSession;
use Thruway\Peer\Client;

abstract class ThruwayProvider
{
    protected function getOnOpenCallback() {
        return function(ClientSession $session) {
            $this->registerProcedures($session);
        };
    }

    public function provideClient(Client $client) {
        $client->on(ThruwayClient::OPEN_EVENT, $this->getOnOpenCallback());
    }

    abstract protected function registerProcedures(ClientSession $session);
}