<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 06.03.2016
 * Time: 04:48
 */

namespace WorldOfEquestria\Service;


use WorldOfEquestria\Websocket\ThruwayClient;

class SubscriptionClient extends ThruwayClient
{
    private $procedureProvider;

    public function __construct($realm, $address, $port, SubscriptionProvider $pc)
    {
        parent::__construct($realm, $address, $port);
        $this->procedureProvider = $pc;
    }

    public function getProvider() {
        return $this->procedureProvider;
    }

    public function start(){
        parent::startClient($this->getProvider());
    }
}