<?php

namespace WorldOfEquestria\Service;

class Authenticator
{
    const AUTH_TYPE = 'ticket';
    const USER_ROLE = 'user';
    const NONE_ROLE = '';

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function getCallback() {
        return function($authData)
        {
            $authData = $this->hydrateData(array_merge((array)$authData[2], ['authid' => $authData[1]])); //Jeśli ktokolwiek stwierdzi że mu coś się tu jebie, to na pewno się jebie :V. I tak to działa i było testowane :*
            echo 'Authenticator: RECEIVED DATA: '.json_encode($authData);
            $isSuccess = $this->authorize($authData);

            return $isSuccess? self::USER_ROLE: self::NONE_ROLE;
        };
    }

    /**
     * @param $authData
     * @return array
     */
    public function hydrateData($authData){
        $peer = explode(':', $authData['transport']->peer);
        return [
            'token'     => $authData['ticket'],
            'id_session' => $authData['session'],
            'authid'     => $authData['authid'],
            'peer'       => [
                'protocol' => $peer[0],
                'ip'       => $peer[1],
            ],
        ];
    }

    /**
     * @param $authData
     * @return bool
     */
    private function authorize($authData)
    {
        $response = $this->apiClient->authorizeToken($authData);

        return $response['is_success'];
    }
}