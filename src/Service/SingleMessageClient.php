<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 25.02.2016
 * Time: 22:22
 */

namespace WorldOfEquestria\Service;


use Thruway\ClientSession;
use WorldOfEquestria\Websocket\ThruwayClient;

//TODO do przebudowy klasa

class SingleMessageClient extends ThruwayClient
{
    public function publishMessage($channel, $message) {
        $client = $this->getClient();
        $client->on('open', function(ClientSession $session) use ($channel, $message, $client){
            $session->publish($channel, $message);
            sleep(1);
            $client->setAttemptRetry(false);
            $session->shutdown();
        });//->then(function () {echo "Publish Published!\n";},function ($error) {echo "Publish Error {$error}\n";});
        $client->start();
    }

    public function registerPublisher($publisherCallback)
    {
        $this->getClient()->setAttemptRetry(false);
        $this->getClient()->getLoop()->addPeriodicTimer(0.1, $publisherCallback);
    }

    public function start()
    {
        parent::startClient();
    }
}