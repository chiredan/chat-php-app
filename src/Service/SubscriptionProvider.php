<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 06.03.2016
 * Time: 04:49
 */

namespace WorldOfEquestria\Service;


use Exception;
use Thruway\ClientSession;
use WorldOfEquestria\Websocket\ThruwayProvider;

class SubscriptionProvider extends ThruwayProvider
{
    const CALLEE_PROCEDURE = 1;
    const CALL_PROCEDURE  = 2;

    private $procedures = [
        self::CALLEE_PROCEDURE => [],
        self::CALL_PROCEDURE   => [],
    ];

    public function __construct()
    {}

    public function registerProcedure($name, callable $proc) {
        if(isset($this->procedures[self::CALLEE_PROCEDURE][$name])) {
            throw new Exception('Procedure already Exists!');
        }

        $this->procedures[self::CALLEE_PROCEDURE][$name] = $proc;

        return $this;
    }

    public function callProcedure($name, callable $proc) {
        $this->procedures[self::CALL_PROCEDURE][$name] = $proc;
    }

    protected function registerProcedures(ClientSession $session)
    {
        foreach($this->procedures[self::CALLEE_PROCEDURE] as $name => $procedure){
            $session->subscribe($name, $procedure);
        }

        foreach($this->procedures[self::CALL_PROCEDURE] as $name => $procedure){
            $session->publish($name, $procedure);
        }
    }
}