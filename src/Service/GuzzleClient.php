<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 21.02.2016
 * Time: 00:01
 */

namespace WorldOfEquestria\Service;

use Guzzle\Http\Client;

abstract class GuzzleClient
{
    protected $client;

    /**
     * GuzzleClient constructor.
     * @see http://guzzle3.readthedocs.org/http-client/client.html#creating-a-client
     *
     * @param $url
     * @param $isSecure
     * @param array $config
     */
    public function __construct($url, $isSecure = false, $config = [])
    {
        $url = sprintf('http%s://%s/', ($isSecure? 's':''), $url);
        $this->client = new Client($url);
    }

    public function postRequest($path, $body = null, $headers = null, $options = []) {
        return $this
            ->client
            ->post($path, $headers, $body, $options)
            ->send()
            ->json();
    }

    public function getRequest($path, $data = null, $headers = null, $options = null){

        if(null !== $data){
            $path .= '?';

            $keys = array_keys($data);
            $last = end($keys);
            foreach($data as $key=>$value){
                $path.=$key.'='.$value.(($key === $last)? '':'&');
            }
        }

        return $this
            ->client
            ->get($path, $headers, $options)
            ->send()
            ->json();
    }
}