<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 21.02.2016
 * Time: 15:57
 */

namespace WorldOfEquestria\Command;

use Predis\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WorldOfEquestria\Service\RpcClient;

class ListenerCommand extends ContainerCommand
{
    const COMMAND_NAME = 'worldofequestria:listener';
    const CHANNEL_NAME = 'woe.call';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var Client $redis
         */
        $redis = $this->getContainer()->get('snc_redis.default');
        $key   = $this->getContainer()->getParameter('chat.message.queue');

        $callback = function ($dummyArg, $msg, $option) use ($redis, $key) {
            $msg->id_session = $option->caller;
            $msg->created_at = date('Y-m-d H:i:s');
            
            if(empty(trim($msg->message))) 
                return false;

            $redis->rpush($key, json_encode($msg));

            return true;
        };

        /** @var RpcClient $client */
        $client = $this->getContainer()->get('rpc_client');

        $client->getProvider()->registerProcedure(self::CHANNEL_NAME, $callback);

        $client->start();
    }
}