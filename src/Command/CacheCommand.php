<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 06.03.2016
 * Time: 04:42
 */

namespace WorldOfEquestria\Command;

use Predis\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WorldOfEquestria\Service\ApiClient;
use WorldOfEquestria\Service\RpcClient;
use WorldOfEquestria\Service\SubscriptionClient;

class CacheCommand extends WorkerCommand
{
    const COMMAND_NAME = 'worldofequestria:cache';
    const DELETE_CALL  = 'woe.internal.delete';
    const INSERT_CALL  = 'woe.internal.insert';
    const CACHE_PREFIX = 'channel_';

    /** @var  Cache */
    private $cache;

    /**
     * @var SubscriptionClient
     */
    private $client;


    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cache  = $this->generateCache();
        /** @var RpcClient $rpcProvider */
        $this->client = $this->getContainer()->get('rpc_client');
        $this->client
            ->getProvider()
                ->registerProcedure(self::DELETE_CALL, $this->getDeleteCallback())
                ->registerProcedure(self::INSERT_CALL, $this->getInsertCallback())
        ;
        $this->client->start();
    }

    /**
     * @return Cache
     */
    private function generateCache()
    {
        /**
         * @var ApiClient $apiClient
         * @var Client $redis
         */
        $apiClient = $this->getContainer()->get('api_client');
        $redis     = $this->getContainer()->get('snc_redis.cache');
        return new Cache($apiClient, $redis);
    }

    /**
     * @return \Closure
     */
    private function getDeleteCallback()
    {
        return function($args) {
            $channel   = $args[0];
            $messageId = $args[1];
            echo 'DELETE MESSAGE id: '.$messageId;

            return $this->cache->delete($channel, $messageId);
        };
    }

    /**
     * @return \Closure
     */
    private function getInsertCallback()
    {
        return function($args) {

            $channel = $args[0];
            $msg     = json_encode($args[1]);

            echo "INSERT {$channel} MESSAGE:";
            print_r($msg);
            echo PHP_EOL;

            return $this->cache->push($channel, $msg);
        };
    }
}

class Cache {

    const BUFFER_SIZE   = 50;
    const BUFFER_OUTPUT = 20;

    private $messages = [];

    /**
     * @var Client
     */
    private $redis;

    /**
     * Cache constructor.
     * @param ApiClient $client
     * @param Client $redis
     *
     * @throws \Exception
     */
    public function __construct(ApiClient $client, Client $redis)
    {

        $this->redis = $redis;
        echo "Create Cache instance \n";
        echo "Getting channel list \n";
        $channelsResponse = $client->getChannelList();
        echo "Channel list: \n";
        print_r($channelsResponse);
        echo PHP_EOL;

        if(false === $channelsResponse['is_success']){
            throw new \Exception('Api trouble');
        }

        $this->getMessages($client, $channelsResponse['channels']);
        echo 'Creating Cache done'.PHP_EOL;
    }

    /**
     * @param ApiClient $client
     * @param $channels
     */
    private function getMessages(ApiClient $client, $channels)
    {
        echo 'Getting messages BEGIN'.PHP_EOL;
        foreach($channels as $channel){
            echo "Memory usage before: " . (memory_get_usage() / 1024) . " KB" . PHP_EOL;
            echo 'Messages for channel: '.$channel.PHP_EOL;
            $apiResponse = $client->getMessages($channel);

            if(false === $apiResponse['is_success']) {
                $messages = [];
            } else {
                $messages = $apiResponse['messages'];
            }
            $messages = array_slice($messages, -50, 50, true);
            print_r($messages);
            echo 'Get '.count($messages).' messages'.PHP_EOL;
            $this->messages[$channel] = $messages;
            $this->refreshCache($channel);
            echo 'Channel cached'.PHP_EOL;
            sleep(1);
        }
        echo 'Getting messages END'.PHP_EOL;
    }

    /**
     * @param $channel
     * @param $msg
     * @return bool
     */
    public function push($channel, $msg) {

        if(false === isset($this->messages[$channel]) || false === is_array($this->messages[$channel])){
            $this->messages[$channel] = [];
        }

        $messageId = (string)json_decode($msg, true)['message_id'];

        $this->messages[$channel][$messageId] = $msg;
        $this->refreshCache($channel);

        return true;
    }

    /**
     * @param $channel
     */
    public function refreshCache($channel){

        if(false === isset($this->messages[$channel]) || false === is_array($this->messages[$channel]) ) {
            return;
        }

        if(count($this->messages[$channel]) > 50) {
            $this->messages[$channel] = array_slice($this->messages[$channel], -50, 50, true);
        }
        print_r($this->messages[$channel]);
        $this->redis->set(CacheCommand::CACHE_PREFIX.$channel, json_encode($this->messages[$channel]));
    }

    /**
     * @param $channel
     * @param $id
     * @return bool
     */
    public function delete($channel, $id){

        if(false === isset($this->messages[$channel]) || null === $this->messages[$channel]){
            return false;
        }

        echo "UNSET MESSAGE".PHP_EOL.$this->messages[$channel][(string)$id].PHP_EOL;
        unset($this->messages[$channel][(string)$id]);

        $this->refreshCache($channel);
        return true;
    }
}