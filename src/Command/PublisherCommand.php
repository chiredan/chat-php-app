<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 25.02.2016
 * Time: 00:01
 */

namespace WorldOfEquestria\Command;

use Predis\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Thruway\AbstractSession;
use WorldOfEquestria\Service\ApiClient;
use WorldOfEquestria\Service\SingleMessageClient;

//TODO do przebudowy
class PublisherCommand extends WorkerCommand
{
    const COMMAND_NAME = 'worldofequestria:publisher';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Running Worker');

        /** @var SingleMessageClient $client */
        $client = $this->getContainer()->get('single_message_client');
        $client->registerPublisher($this->getPublisherCallback($output, $client->getClient()));
        $client->start();
    }

    /**
     * @param OutputInterface $output
     * @return callable
     */
    private function getPublisherCallback(OutputInterface $output, \Thruway\Peer\Client $client){ // TODO do serwisów
        /**
         * @var Container $container
         * @var Client $redis
         * @var ApiClient $apiClient
         */
        $container = $this->getContainer();
        $redis     = $container->get('snc_redis.default');
        $apiClient = $container->get('api_client');
        $key       = $container->getParameter('chat.message.queue');
        $voter     = $this->getVoter();

        return function() use($redis, $key, $apiClient, $voter, $output, $client){
            static $isSending;

            if(null === $client->getSession() || AbstractSession::STATE_UP !== $client->getSession()->getState()){
                //Jako że timery działają przed ustanowieniem połączenia pomijam kolejkę tak by uniknąć pracy na nullach albo niepełnych połączeniach
                return;
            }

            if(true === $isSending){
                $isSending = false;
                return;
            }
            static $session;
            if (null === $session) {
                $session = $client->getSession();
            }
            $msg = $redis->lpop($key);
            $output->writeln('<info>MSG:</info>' . $msg);
            $msg = json_decode($msg, true);

            if (null === $msg) {
                $output->writeln('<info>Empty message, sleep</info>');
                sleep(1);
                return;
            }

            if (false === $this->checkMessage($msg)) {
                $output->writeln('<error>Message bad format</error>');
                sleep(1);
                return;
            }

            $client = $apiClient->getUserData($msg['id_session']);
            $output->writeln('<info>Client:</info> ' . json_encode($client));

            if (false === $voter->vote($client, $msg)) {
                $output->writeln('<error>Client Access Denied</error>');
                sleep(1);
                return;
            }

            $msg['payload']['user_data'] = $client;

            if(isset($msg['message'])){
                $msg['message'] = htmlspecialchars(trim($msg['message']), ENT_COMPAT, 'UTF-8');
            }

            $response = $apiClient->publishMessage($msg);
            
            $output->writeln('<info>Publish message:</info> ' . json_encode($msg));
            if ($response['is_success']) {
                $msg['message_id'] = $response['message_id'];

                try {
                    if (substr($msg['message'], 0, 7) === '/DELETE') {
                        $msgCall = [
                            'channel' => $msg['channel'],
                            'type' => 'delete',
                            'params' => [
                                'channel' => $msg['channel'],
                                'message_id' => trim(substr($msg['message'], 7)),
                            ]
                        ];

                        $session->call(CacheCommand::DELETE_CALL, [$msgCall['params']['channel'], $msgCall['params']['message_id']]);

                        $apiClient->hideMessage($msgCall['params']['message_id']);
                    } else {
                        $session->call(CacheCommand::INSERT_CALL, [$msg['channel'], $msg]);
                    }
                } catch(\Exception $e) {
                    $output->writeln('<error>Omit caching</error>');
                }

                $session->publish($msg['channel'], [json_encode($msg)]);
                $isSending = true;
            }
        };
    }

    private function getVoter()
    {
        return new PublishVoter();
    }

    private function checkMessage(array $message) {
        foreach (['channel', 'payload', 'message', 'id_session'] as $key) {
            if(false === isset($message[$key]))

                return false;
        }

        return true;
    }
}

class PublishVoter {

    private $voters = [
        'isSuccess',
        'isBanned',
        'checkChannel',
        'checkRoleplay',
        'deleteMessage'
    ];

    public function vote($client, $msg){
        foreach($this->voters as $voter) {
            if(false === $this->{$voter}($client, $msg)) {
                return false;
            }
        }

        return true;
    }

    public function isSuccess($client, $msg)
    {
        return $client['is_success'];
    }
    
    public function isBanned($client, $msg)
    {
        return !$client['is_banned'];
    }
    
    private function checkChannel($client, $msg) {
        $parts = explode('.', $msg['channel']);

        return ($parts[0] === 'woe' && in_array($parts[1], ['chat', 'roleplay']));
    }

    private function checkRoleplay($client, $msg){
        $parts = explode('.', $msg['channel']);

        return !('roleplay' === $parts[1] && empty($client['ponies']));
    }
    
    private function deleteMessage($client, $msg){
        
        if(substr($msg['message'],0,7) === '/DELETE') {
            foreach (['ROLE_ADMIN','ROLE_MODERATOR','ROLE_RPMODERATOR','ROLE_SUPERADMIN'] as $role) {
                if(in_array($role, $client['roles']))
                    return true;
            }
            return false;   
        }
        
        return true;
    }
}