<?php

namespace WorldOfEquestria\Websocket;

use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;
use WorldOfEquestria\Service\ProcedureProvider;

abstract class ThruwayClient
{
    const OPEN_EVENT  = 'open';
    const CLOSE_EVENT = 'close';

    protected $realm;
    private $address;
    /**
     * @var Client
     */
    private $client;

    public function __construct($realm, $address, $port)
    {
        $this->client  = $this->getNewClient($realm, $address, $port);
        $this->address = $address;
    }

    public function getClient(){
        return $this->client;
    }

    public function setClient(Client $client){
        $this->client = $client;
    }

    /**
     *
     */
    protected function startClient(ThruwayProvider $ep = null){

        if($ep !== null){
            $ep->provideClient($this->client);
        }

        $this->client->start();
    }

    public function getNewClient($realm, $address, $port) {
        $client = new Client($realm);
        $address = sprintf('ws://%s:%s/', $address, $port);
        $client->addTransportProvider(new PawlTransportProvider($address));

        return $client;
    }
}