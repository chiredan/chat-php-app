<?php
/**
 * Created by PhpStorm.
 * User: Pawel
 * Date: 18.02.2016
 * Time: 01:21
 */

namespace WorldOfEquestria\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WorldOfEquestria\Service\Authenticator;
use WorldOfEquestria\Service\RpcClient;

class AuthenticatorCommand extends ContainerCommand
{
    const COMMAND_NAME = 'worldofequestria:authenticator';
    const CHANNEL_NAME = 'woe.internal.authenticate';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Authenticator $authenticator */
        $authenticator = $this->getContainer()->get('authenticator');
        /** @var RpcClient $client */
        $client = $this->getContainer()->get('rpc_client');

        $client->getProvider()->registerProcedure(self::CHANNEL_NAME, $authenticator->getCallback());

        $client->start();
    }
}