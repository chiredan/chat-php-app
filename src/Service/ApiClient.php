<?php

namespace WorldOfEquestria\Service;


class ApiClient
{
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }


    /**
     * @example<pre>
     *  {
     *      'is_success' => true
     *  }
     * </pre>
     *
     * @param $authData
     * @return array
     */
    public function authorizeToken($authData)
    {
        $ch = curl_init($this->url.'/internal/authorizeToken');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($authData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($authData))));

        echo "API method: authorizeToken, send data: ".json_encode($authData);
        if(!($response = curl_exec($ch))) {
            $response = '{"is_success":false}';
        }

        echo "API method: authorizeToken, received:".$response;
        return json_decode($response, true);
    }

    /**
     * @example<pre>
     *  {
     *      'is_success' : true,
     *      'username' : 'Thelleo',
     *      'avatar' : 'blybly.jpg'
     *      'ponies' : [
     *          1: {'ponyname' : 'Penthiny', 'sex' : 1, 'avatar' : 'blybly2.jpg'}
     *      ],
     *      'is_banned' : false
     *  }
     * </pre>
     *
     * @param $idSession
     * @return array
     */
    public function getUserData($idSession)
    {
        $ch = curl_init($this->url.'/internal/getUserData');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['id_session' => $idSession]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode(['id_session' => $idSession]))));

        if(!($response = curl_exec($ch))){
            $response = '{"is_success":false}';
        }

        return json_decode($response, true);
    }

    /**
     *
     * </pre>
     * @param $msg
     * @return array
     */
    public function publishMessage($msg)
    {

            $ch = curl_init($this->url.'/internal/publishMessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($msg));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($msg))));

            if(!($response = curl_exec($ch))){
                $response = '{"is_success":false}';
            }

            return json_decode($response, true);
    }

    public function getChannelList()
    {
        $ch = curl_init($this->url.'/internal/getChannels');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if(!($response = curl_exec($ch))){
            $response = '{"is_success":false}';
        }

        echo "Api Response:".PHP_EOL;
        print_r($response);

        return json_decode($response, true);
    }

    public function getMessages($channel) {
        $msg = json_encode(['channel' => $channel, 'limit' => 50]);

        $ch = curl_init($this->url.'/internal/getMessages');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($msg)));

        if(!($response = curl_exec($ch))){
            $response = '{"is_success":false}';
        }

        return json_decode($response, true);
    }

    public function hideMessage($message_id)
    {
        $msg = json_encode(['id_message' => $message_id, 'is_hidden' => true]);

        $ch = curl_init($this->url.'/internal/hideMessage');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($msg)));

        if(!($response = curl_exec($ch))){
            $response = '{"is_success":false}';
        }

        return json_decode($response, true);
    }
}
